const http=require('http')
let url=require("url")

// Creates a variable 'port' to store the port number
const port=3000

const server=http.createServer((request,response)=>{
	if(request.url=='/login'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('Welcome to the login page')

		//Accesing the 'homepage' route returns a message of "This is homepage" 
	}else{
		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

// uses the "Server" and "port"  variables created above
server.listen(port)
console.log(`Server now accesible at localhost:${port}`)